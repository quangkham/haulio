//
//  JobTableViewCell.swift
//  Haulio
//
//  Created by Quang Kham on 11/16/18.
//  Copyright © 2018 Quang Kham. All rights reserved.
//

import UIKit
import GoogleSignIn

class JobTableViewCell: UITableViewCell {

    var parent:JobAvaliableViewController?
    var job:Job!
    
    @IBOutlet weak var jobNumberValue: UILabel!
    
    @IBOutlet weak var companyNameValue: UILabel!
    
    @IBOutlet weak var addressValue: UILabel!
    
    @IBOutlet weak var acceptBtn: UIButton!
    
    @IBAction func acceptJob(_ sender: UIButton) {
     
        self.parent?.performSegue(withIdentifier: "mapsegue", sender: job )
       // GIDSignIn.sharedInstance()?.currentUser
    }
    
    func configure(_ content:Job){
        self.job =  content
        
        jobNumberValue.text = String(content.jobNumber)
        companyNameValue.text = content.companyName
        addressValue.text = content.address
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        acceptBtn.layer.cornerRadius = 0.7
        
        // Initialization code
    }
    override func layoutSubviews() {
        acceptBtn.layer.cornerRadius = 0.7
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
