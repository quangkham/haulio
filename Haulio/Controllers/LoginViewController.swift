//
//  LoginViewController.swift
//  Haulio
//
//  Created by Quang Kham on 11/16/18.
//  Copyright © 2018 Quang Kham. All rights reserved.
//

import UIKit
import GoogleSignIn

class LoginViewController: UIViewController,GIDSignInDelegate,GIDSignInUIDelegate {

    
    
    @IBAction func login(_ sender: UIButton) {
        
       
        GIDSignIn.sharedInstance().signIn()
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate =  self
        // Do any additional setup after loading the view.
    }
    
    //MARK: Google Sign Call Back
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
           print(error.localizedDescription)
            return
        }
         print("Login success")
        performSegue(withIdentifier: "jobsegue", sender: user)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         let destVC = segue.destination as? JobAvaliableViewController
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
