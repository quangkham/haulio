//
//  MapViewController.swift
//  Haulio
//
//  Created by Quang Kham on 11/16/18.
//  Copyright © 2018 Quang Kham. All rights reserved.
//

import UIKit
import  GoogleSignIn
import GoogleMaps
import GooglePlaces

import Kingfisher
import CoreLocation

class MapViewController: UIViewController , GMSAutocompleteViewControllerDelegate, UISearchBarDelegate {
    
    
    var mapPlaceAutoCompleteVC:GMSAutocompleteViewController = GMSAutocompleteViewController ( )
    
    @IBOutlet weak var searchPlaceBar: UISearchBar!
    @IBOutlet weak var mapView:GMSMapView!
    var job:Job!
    var currentUserLocation:CLLocation?
    
    var user:GIDGoogleUser?
    @IBOutlet weak var profileImage: UIImageView!
    
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var userJobID: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getNavImage()
        user =  GIDSignIn.sharedInstance()?.currentUser
        userName.text = user?.profile.givenName ?? "Unknown"
        let url =  user?.profile.imageURL(withDimension: 200)
        profileImage.kf.setImage(with: url)
        profileImage.kf.indicatorType = .activity
        profileImage.layer.cornerRadius =  profileImage.layer.frame.size.width /  2
        profileImage.clipsToBounds = true
        userJobID.text =  String(job?.jobNumber ??  0 )
        
        setLocation( job.geolocation?.coordinate)
        setUserLocation( )
        mapPlaceAutoCompleteVC.delegate = self 
        searchPlaceBar.delegate = self
        mapPlaceAutoCompleteVC.view.backgroundColor = UIColor.clear
        }
    
    func getNavImage(){
        
        let navController = navigationController!
        
        let image = UIImage(named: "setTitle")
        let imageView = UIImageView(image: image)
        
        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height
        
        let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
        let bannerY = bannerHeight / 2 - (image?.size.height)! / 2
        
        imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth, height: bannerHeight)
        imageView.contentMode = .center
        navigationItem.titleView = imageView
        
        
        let logOutImage = UIImage(named: "logout")
        
        let imageLogOut = UIImageView(image: logOutImage)
        
        let logOutBtn = UIBarButtonItem(image: logOutImage, style: UIBarButtonItem.Style.plain, target: self, action: #selector(logout))
        
        self.navigationItem.rightBarButtonItem = logOutBtn
        
    }
    @objc func logout(_ sender:UIBarButtonItem) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
 
    func setLocation(_  position:CLLocationCoordinate2D?) {
        if let position = position {
        let marker = GMSMarker()
            
            marker.position = position
            marker.map = self.mapView
            mapView.animate(toZoom: 13.0)
            mapView.animate(toLocation: position)
            
        }
    }
    
        func setUserLocation() {
            if let position = currentUserLocation?.coordinate {
                let marker = GMSMarker()
             
                marker.position = position
                marker.map = self.mapView
                mapView.animate(toZoom: 13.0)
                mapView.animate(toLocation: position)
                
            }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
       self.present(   mapPlaceAutoCompleteVC, animated: true)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.mapPlaceAutoCompleteVC.dismiss(animated: true) {
            let location = place.coordinate
            self.setLocation(location)
        }
       
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
         self.mapPlaceAutoCompleteVC.dismiss(animated: true)
    }
    

   

}
