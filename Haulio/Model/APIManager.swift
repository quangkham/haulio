    

    import Foundation
  import Alamofire
 
    class APIManager:NSObject {

        static var main          = APIManager()
        static let baseURLString = "https://api.myjson.com/"
        

        private enum EndPoint: String {

            //Registration & Sign In
            case jobdata                 = "bins/8d195.json" //DOC
            

            var url:URL  {
                //Forcely return as of all API end Points are confirmed
                return URL(string: APIManager.baseURLString + rawValue) ?? URL(string:"https://www.google.com")!
            }

            var method: HTTPMethod {
                switch self {
                case .jobdata: return .get

                default:return .post  //TODO: Check all request are set as Post, unless specified
                }
            }
        }

      
        func getJob( onSuccess:(([[String:Any]] )->())? = nil , onError:((Error)->())? = nil  ) {
            var parameters:Parameters = [:]
            
            Alamofire.request(EndPoint.jobdata.url, method:EndPoint.jobdata.method, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response ) in
                
                if let error = response.error {
                     onError?(error)
                } else {
                    if let responseDict =  response.value as? [[String:Any]] {
                         onSuccess?(responseDict)
                    }
                    
                }
            }
        	}
      
            

        
    }

